import React, { Component} from 'react';
import { Platform, View, Text, StyleSheet, TouchableOpacity, TouchableWithoutFeedback } from 'react-native';
import { BleManager } from 'react-native-ble-plx';
import ActionButton from 'react-native-action-button';
import Icon from 'react-native-vector-icons/Ionicons';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'space-evenly',
        alignItems: 'center',
        height: 50
    },
    touchElement: {
        height: 20
    }
});

export default  class BluetoothComponent extends Component {

    constructor() {
        super();
        this.manager = new BleManager();
        this.state={info:""};
        this.prefixUUID = "";
        this.suffixUUID = "";
    }

    //Wait for the BLE stack to init properly on iOS
    componentWillMount() {
        if (Platform.OS === 'ios') {
            this.manager.onStateChange((state) => {
                if (state === 'PoweredOn') this.scanAndConnect()
            })
        } else {
            this.scanAndConnect()
        }
    }


    info(message){
        this.setState({info: message});
    }

    scanAndConnect(){
        this.manager.startDeviceScan(null, null, (error, device) => {
            console.log("Scan and conenct");
            console.log(device);
            info(device);
            return;

            if(error){
                console.log(error);
                return;
            }

        })
    }

    startFrontAxisUp()  {
        console.log("Start Front Axis Up");
    }
    stopFrontAxisUp()  {
        console.log("Stop Front Axis Up");
    }
    startFrontAxisDown()  {
        console.log("Start Front Axis Down");
    }
    stopFrontAxisDown()  {
        console.log("Stop Front Axis Down");
    }
    startBackAxisUp()  {
        console.log("Start Back Axis Up");
    }
    stopBackAxisUp()  {
        console.log("Stop Back Axis Up");
    }
    startBackAxisDown()  {
        console.log("Start Back Axis Down");
    }
    stopBackAxisDown()  {
        console.log("Stop Back Axis Down");
    }



    render(){
        return(
            <View>
                <Text>Bluetooth module state:</Text>
                <Text>
                    {this.state.info}
                </Text>

                <View style={styles.container}>
                    <TouchableOpacity style={styles.touchElement} onPressIn={this.startFrontAxisUp} onPressOut={this.stopFrontAxisUp}>
                        <View><Text>Első tengely fel</Text></View>
                    </TouchableOpacity>

                    <TouchableOpacity style={styles.touchElement} onPressIn={this.startFrontAxisDown} onPressOut={this.stopFrontAxisDown}>
                        <View><Text>Első tengely le</Text></View>
                    </TouchableOpacity>

                    <TouchableOpacity style={styles.touchElement} onPressIn={this.startBackAxisUp} onPressOut={this.stopBackAxisUp}>
                        <View><Text>Hátsó tengely fel</Text></View>
                    </TouchableOpacity>

                    <TouchableOpacity style={styles.touchElement} onPressIn={this.startBackAxisDown} onPressOut={this.stopBackAxisDown}>
                        <View><Text>Hátsó tengely le</Text></View>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}