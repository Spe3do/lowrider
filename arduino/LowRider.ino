/*
 * LowRider Project
 * 
 * 
 * Arduino project for controlling 4 relay with bluetooth LE
 * 
 * INPUT: bluetooth v4.0 LE TX and RX pins
 *    We are expecting two byte codes from the Bluetooth
 *    INPUT MAP:
 *        - xy where  
 *              x is the relay index (1-4)
 *              y is the state of the relay
 *                  0: set the pin state to HIGH (turn off the relay)        
 *                  0: set the pin state to LOW (turn on the relay)
 * 
 * OUTPUT: Digital 8-11 Pins to the relay
 *    The relay will turn on when we are pulling down to the desider pin so:
 *      HIGH will turn off the relay
 *      LOW will turn on the relay
 * 
 * 
*/

//Relay pins 
int relayPin1 = 8;
int relayPin2 = 9;
int relayPin3 = 10;
int relayPin4 = 11;

void setup() {
  //Setup the relay pins for output
  pinMode(relayPin1, OUTPUT);
  pinMode(relayPin2, OUTPUT);
  pinMode(relayPin3, OUTPUT);
  pinMode(relayPin4, OUTPUT); 

  //Turn off the relays
  digitalWrite(relayPin1, HIGH);
  digitalWrite(relayPin2, HIGH);
  digitalWrite(relayPin3, HIGH);
  digitalWrite(relayPin4, HIGH);

  //Setup the serial communication for the  Bluetooth
  Serial.begin(9600);
}

void loop() {
  //Wait until there is anything sent by Bluetooth     
  if(Serial.available() > 0) {
    //We are expecting the relay index and the state of the relay in the buffer
    char buffer[] = {' ',' '};
    Serial.readBytesUntil('n', buffer, 2);
    int inputNumber = atoi(buffer);

    if(inputNumber >= 10 && inputNumber <= 42) {
      switchRelay(inputNumber);
    } else  {
      Serial.println("Wrong data received from Bluetooth");  
    }
  }
}

void switchRelay(int index){
  switch(index){
    case 10:
      digitalWrite(relayPin1,HIGH);
      break;
    case 11:
      digitalWrite(relayPin1,LOW);
      break;
    case 20:
      digitalWrite(relayPin2,HIGH);
      break;
    case 21:
      digitalWrite(relayPin2,LOW);
      break;
    case 30:
      digitalWrite(relayPin3,HIGH);
      break;
    case 31:
      digitalWrite(relayPin3,LOW);
      break;
    case 40:
      digitalWrite(relayPin4,HIGH);
      break;
    case 41:
      digitalWrite(relayPin4,LOW);
      break;
  }
}
